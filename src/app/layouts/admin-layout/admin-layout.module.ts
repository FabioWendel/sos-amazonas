import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatRippleModule} from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSelectModule} from '@angular/material/select';
import { CadastroComponent } from 'app/cadastro/cadastro.component';
import { DemoMaterialModule } from 'app/material-module';
import { LoginComponent } from 'app/login/login.component';
import { NotFoundComponent } from 'app/not-found/not-found.component';
import { LoginService } from 'app/shared/services/login/login.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    DemoMaterialModule,
  ],
  declarations: [
    DashboardComponent,
    CadastroComponent,
    LoginComponent,
    NotFoundComponent,
  ],
  providers: [LoginService],
})

export class AdminLayoutModule {}
