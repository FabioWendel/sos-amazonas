import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Cadastro } from 'app/shared/models/cadastro/cadastro.model';
import { environment } from 'environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CadastroService {

  private apiIbge = environment.apiIbge.databaseURL;
  private apiUrl = environment.dataBaseConfig.databaseURL;



  constructor(private httpClient: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  getMunicipios(): Observable<any> {
    return this.httpClient.get(this.apiIbge);
  }

  getOrders(page = 0): Observable<any> {
    return this.httpClient.get<Cadastro[]>(`${this.apiUrl}orders`, {params:{page: page+''}})
      .pipe(
        retry(2),
        catchError(this.handleError))
  }

  getOrderById(id: number): Observable<Cadastro> {
    return this.httpClient.get<Cadastro>( `${this.apiUrl}orders/${id}`)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  saveOrder(cad: Cadastro): Observable<Cadastro> {
    console.log(cad)
    return this.httpClient.post<Cadastro>(`${this.apiUrl}orders`,JSON.stringify(cad),this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  updateOrder(id:Cadastro, cad: Cadastro): Observable<Cadastro> {
    return this.httpClient.put<Cadastro>(`${this.apiUrl}orders/${id.id}`,JSON.stringify(cad), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  deleteOrder(cad: Cadastro) {
    return this.httpClient.delete<Cadastro>( `${this.apiUrl}orders/${cad.id}`, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };

}
