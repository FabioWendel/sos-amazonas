import { Component, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Cadastro } from 'app/shared/models/cadastro/cadastro.model';
import { CadastroService } from 'app/shared/services/cadastro/cadastro.service';
import { error } from 'jquery';
import { ToastrService } from 'ngx-toastr';
import { fromEvent, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {PageEvent} from '@angular/material/paginator';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.scss'],
})
export class CadastroComponent implements OnInit {

  displayedColumns: string[] = ['termo', 'municipio', 'local', 'data', 'opcao'];
  cadastros: MatTableDataSource<Cadastro>;

  userform: FormGroup;
  isLoading = true;

  @ViewChild('myTemplate') customTemplate: TemplateRef<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  submitted: boolean;
  controlDelete = false;
  cadastroKey: any;
  controlSubmit: boolean = false;

  imgSrc: any;
  controlPreviewFormat: boolean = false;
  municipios: any[] = [];
  filteredListMunicipios: [];
  dataFormItem: any;

  filteredListItems: any[];


  unidadeDeMedida: any[];


  filterInputRow = {};
  data: { id: string; termo: string; municipio: string; data: string; item: string; quantidade: string; medida: string; local: string; evidencia: string; }[];
  totalPages: any;


  constructor(
    public dialog: MatDialog,
    private fb: FormBuilder,
    private cadastroService: CadastroService,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    this.getUnidadeMedida();
    this.getItemsSelected();
    this.getMunicipios();
    this.getCadastros();
    this.userform = this.fb.group({
      'termo': new FormControl('', Validators.required),
      'municipio': new FormControl('', Validators.required),
      'data': new FormControl('', Validators.required),
      'items': this.fb.array([]),
      'local': new FormControl('', Validators.required),
      'evidencia': new FormControl(''),
      'observacao': new FormControl(''),
    });
  }


  items() : FormArray {
    return this.userform.get("items") as FormArray
  }
   
  newItem(): FormGroup {
    return this.fb.group({
      'name': new FormControl('', Validators.required),
      'quantidade': new FormControl('', Validators.required),
      'medida': new FormControl('', Validators.required)
    })
  }

  getItem(): FormGroup {
    return this.fb.group({
      'name': new FormControl(this.dataFormItem.items, Validators.required),
      'quantidade': new FormControl(this.dataFormItem.quantidades, Validators.required),
      'medida': new FormControl(this.dataFormItem.medidas, Validators.required)
    })
  }


  handleClickSelect(i){
    this.filterInputRow[i] = this.filteredListItems;
  }

  inputFilterResert(event, i){
    this.filterInputRow[i] = event
  }

  addItem() {
    this.items().push(this.newItem());
  }

  editItem(){
    this.items().push(this.getItem());
  }
   
  removeItem(i:number) {
    this.items().removeAt(i);
  }

  onSubmit() {
    this.submitted = true;
    if (this.userform.invalid) {
      return;
    }

    if (this.controlSubmit == true) {
      this.postCadastro(this.userform.value);
      this.getCadastros();
      this.dialog.closeAll();
    } else if (this.controlSubmit == false) {
      this.updateCadastro(this.userform.value);
      this.getCadastros();
      this.dialog.closeAll();
    }

  }

  readFile(file: File | Blob): Observable<any> {
    const reader = new FileReader();
    let loadend = fromEvent(reader, 'loadend').pipe(
      map((read: any) => {
        return read.target.result;
      })
    );
    reader.readAsDataURL(file);
    return loadend;
  }

  onFileChange(fileList: FileList) {
    this.readFile(<File>fileList[0]).subscribe(res => {
      this.controlPreviewFormat = fileList[0].type == 'application/pdf' ? true : false;
      this.imgSrc = res;
      this.userform.controls['evidencia'].setValue(res);
    });
  }

  getMunicipios() {
    this.cadastroService.getMunicipios().subscribe((municipios: any) => {
      this.municipios = municipios;
      this.filteredListMunicipios = municipios;
    });
  }


  getUnidadeMedida(){
    this.unidadeDeMedida = [
      {value: "UNIDADE"},
      {value: "CAIXA"},
      {value: "PACOTE"},
    ];
  }


  getItemsSelected(){
    this.filteredListItems = [
      {
        nome: "EPI's",
        items: [
          {value: 'LUVA (CIRÚRGICA - PROCEDIMENTO - DESCARTAVEL)', viewValue: 'LUVA (CIRÚRGICA - PROCEDIMENTO - DESCARTAVEL)'},
          {value: 'MÁSCARA HOSPITALAR', viewValue: 'MÁSCARA HOSPITALAR'},
          {value: 'TOUCA', viewValue: 'TOUCA'},
          {value: 'AVENTAL', viewValue: 'AVENTAL'},
          {value: 'PROTETOR FACIAL - VISEIRA', viewValue: 'PROTETOR FACIAL - VISEIRA'},
          {value: 'PROPÉ', viewValue: 'PROPÉ'},
          {value: 'MACACÃO', viewValue: 'MACACÃO'},
          {value: 'ÓCULOS DE PROTEÇÃO', viewValue: 'ÓCULOS DE PROTEÇÃO'},
        ]
      },
      {
        nome: 'MEDICAMENTO',
        items: [
          {value: 'ACEBROFILINA', viewValue: 'ACEBROFILINA'},
          {value: 'ACETILCISTEÍNA (XAROPE - COMPRIMIDO - SACHÊ - AMPOLA)', viewValue: 'ACETILCISTEÍNA (XAROPE - COMPRIMIDO - SACHÊ - AMPOLA)'},
          {value: 'AMBROSOL', viewValue: 'AMBROSOL'},
          {value: 'AZITROMICINA', viewValue: 'AZITROMICINA'},
          {value: 'CAPTOPRIL', viewValue: 'CAPTOPRIL'},
          {value: 'CARVEDILOL - CARVEDIOL - CARDBET', viewValue: 'CARVEDILOL - CARVEDIOL - CARDBET'},
          {value: 'CEFTRIAXONA', viewValue: 'CEFTRIAXONA'},
          {value: 'CISTEIL', viewValue: 'CISTEIL'},
          {value: 'CLARITROMICINA INJETÁVEL', viewValue: 'CLARITROMICINA INJETÁVEL'},
          {value: 'COMPLEXO B INJETÁVEL', viewValue: 'COMPLEXO B INJETÁVEL'},
          {value: 'DESAMETASONA - DEXASON', viewValue: 'DESAMETASONA - DEXASON'},
          {value: 'DIPIRONA (GOTAS - COMPRIMIDO - ENDOVENOSO - XAPORE)', viewValue: 'DIPIRONA (GOTAS - COMPRIMIDO - ENDOVENOSO - XAPORE)'},
          {value: 'ENOXAPARINA', viewValue: 'ENOXAPARINA'},
          {value: 'FENTANILA', viewValue: 'FENTANILA'},
          {value: 'FLUCANIL', viewValue: 'FLUCANIL'},
          {value: 'IVERMECTINA', viewValue: 'IVERMECTINA'},
          {value: 'MAXAGILNA', viewValue: 'MAXAGILNA'},
          {value: 'LOSARTANA', viewValue: 'LOSARTANA'},
          {value: 'OMEPRAZOL - EUPEPT - TEUTOZOL', viewValue: 'OMEPRAZOL - EUPEPT - TEUTOZOL'},
          {value: 'PARECETAMOL (GOTAS - COMPRIMIDO - ENDOVENOSO)', viewValue: 'PARECETAMOL (GOTAS - COMPRIMIDO - ENDOVENOSO)'},
          {value: 'PIPERACILINA + TAZOBACTAM', viewValue: 'PIPERACILINA + TAZOBACTAM'},
          {value: 'PREDNIZONA', viewValue: 'PREDNIZONA'},
          {value: 'SORO', viewValue: 'SORO'},
          {value: 'VITA A-Z - VITAMINA A-Z', viewValue: 'VITA A-Z - VITAMINA A-Z'},
          {value: 'VITAMINA C', viewValue: 'VITAMINA C'},
          {value: 'VITAMINA D', viewValue: 'VITAMINA D'},
          {value: 'TILEMAX', viewValue: 'TILEMAX'}
        ]
      },
      {
        nome: 'OXIGÊNIO',
        items: [
          {value: 'CILINDRO', viewValue: 'CILINDRO'},
        ]
      },
      {
        nome: 'EQUIPAMENTO',
        items: [
          {value: 'AMBU', viewValue: 'AMBU'},
          {value: 'BIPAP', viewValue: 'BIPAP'},
          {value: 'CPAP', viewValue: 'CPAP'},
          {value: 'CONCENTRADOR DE OXIGÊNIO', viewValue: 'CONCENTRADOR DE OXIGÊNIO'},
          {value: 'KIT VÁLVULA RESPIRADORA DOBRAVEL', viewValue: 'KIT VÁLVULA RESPIRADORA DOBRAVEL'},
          {value: 'KIT VÁLVULA COM FLUXOMETRO', viewValue: 'KIT VÁLVULA COM FLUXOMETRO'},
          {value: 'CATETER NASAL', viewValue: 'CATETER NASAL'},
          {value: 'LÂMINA LARINGO', viewValue: 'LÂMINA LARINGO'},
          {value: 'MANÔMETRO', viewValue: 'MANÔMETRO'},
          {value: 'REANIMADOR MANUAL', viewValue: 'REANIMADOR MANUAL'},
          {value: 'REGULADOR DE PRESSÃO', viewValue: 'REGULADOR DE PRESSÃO'},
          {value: 'KIT RESPIRADOR COM PEDESTAL PARA MONITOR (CABO DE ECG - MAGUEIRA EXTENSORA - BRAÇADEIRA - SENSOR DE OXIMETRIA - SENSOR DE TEMPERATURA)', viewValue: 'KIT RESPIRADOR COM PEDESTAL PARA MONITOR (CABO DE ECG - MAGUEIRA EXTENSORA - BRAÇADEIRA - SENSOR DE OXIMETRIA - SENSOR DE TEMPERATURA)'},
          {value: 'MONITOR MULTIPARAMÉTRICO', viewValue: 'MONITOR MULTIPARAMÉTRICO'},
          {value: 'REANIMADOR MANUAL', viewValue: 'REANIMADOR MANUAL'},
          {value: 'CONEXÃO Y', viewValue: 'CONEXÃO Y'},
          {value: 'MASCARA NÃO REINALAÇÃO', viewValue: 'MASCARA NÃO REINALAÇÃO'},
          {value: 'REANIMADOR MANUAL', viewValue: 'REANIMADOR MANUAL'},
          {value: 'FLUXOMETRO', viewValue: 'FLUXOMETRO'},
          {value: 'UMIDIFICADOR', viewValue: 'UMIDIFICADOR'},
          {value: 'REGULADOR DE PRESSÃO', viewValue: 'REGULADOR DE PRESSÃO'},
          {value: 'VÁLVULA DE EXALAÇÃO', viewValue: 'VÁLVULA DE EXALAÇÃO'},
          {value: 'VENTILADOR MECÂNICO', viewValue: 'VENTILADOR MECÂNICO'},
          {value: 'CIRCUITO DUPLO', viewValue: 'CIRCUITO DUPLO'},
        ]
      },
      {
        nome: 'MATERIAL/INSUMO',
        items: [
          {value: 'ÁGUA SANITÁRIA', viewValue: 'ÁGUA SANITÁRIA'},
          {value: 'AGULHA', viewValue: 'AGULHA'},
          {value: 'ALCOOL', viewValue: 'ALCOOL'},
          {value: 'KIT SANIZANTE (DESINFETANTE HOSPITALAR + PULVERIZADOR)', viewValue: 'KIT SANIZANTE (DESINFETANTE HOSPITALAR + PULVERIZADOR)'},
          {value: 'APARELHO DE PRESSÃO', viewValue: 'APARELHO DE PRESSÃO'},
          {value: 'ATADURA', viewValue: 'ATADURA'},
          {value: 'CADEIRA DE BANHO', viewValue: 'CADEIRA DE BANHO'},
          {value: 'CADEIRA DE RODAS', viewValue: 'CADEIRA DE RODAS'},
          {value: 'DESINFETANTE', viewValue: 'DESINFETANTE'},
          {value: 'DETERGENTE', viewValue: 'DETERGENTE'},
          {value: 'ESCADA PARA MACA', viewValue: 'ESCADA PARA MACA'},
          {value: 'ESPONJA DE LIMPEZA', viewValue: 'ESPONJA DE LIMPEZA'},
          {value: 'ESPUMA HIGIENIZADORA', viewValue: 'ESPUMA HIGIENIZADORA'},
          {value: 'FRALDA', viewValue: 'FRALDA'},
          {value: 'KIT NEBULIZAÇÃO', viewValue: 'KIT NEBULIZAÇÃO'},
          {value: 'KIT LARINGOSCÓPIO', viewValue: 'KIT LARINGOSCÓPIO'},
          {value: 'LENÇOL HOSPITALAR DE PAPEL', viewValue: 'LENÇOL HOSPITALAR DE PAPEL'},
          {value: 'MÁSCARA DE TECIDO', viewValue: 'MÁSCARA DE TECIDO'},
          {value: 'OXÍMETRO', viewValue: 'OXÍMETRO'},
          {value: 'PILHA', viewValue: 'PILHA'},
          {value: 'ALSERINGACOOL', viewValue: 'SERINGA'},
          {value: 'SONDA', viewValue: 'SONDA'},
          {value: 'SUPORTE PARA SORO', viewValue: 'SUPORTE PARA SORO'},
          {value: 'TERMOMETRO - SENSOR DE TEMPERATURA', viewValue: 'TERMOMETRO - SENSOR DE TEMPERATURA'},
          {value: 'TUBO ENDOTRAQUIAL', viewValue: 'TUBO ENDOTRAQUIAL'},
          {value: 'UMIDIFICADOR', viewValue: 'UMIDIFICADOR'},
          {value: 'VÁLVULA DE CILINDRO', viewValue: 'VÁLVULA DE CILINDRO'}
        ]
      },
      {
        nome: 'ALIMENTOS',
        items: [
          {value: 'ÁGUA MINERAL', viewValue: 'ÁGUA MINERAL'},
          {value: 'CESTA BÁSICA', viewValue: 'CESTA BÁSICA'},
          {value: 'FARDO DE MILITOS', viewValue: 'FARDO DE MILITOS'},
        ]
      }
    ];
  }

  getCadastros(): void {
    this.cadastroService.getOrders().subscribe(data => {
      const {cadastros, totalPages}  =  data;
      this.totalPages = totalPages;
      this.cadastros = new MatTableDataSource(cadastros);
      // this.paginator.pageIndex = 0;
      // this.cadastros.paginator = this.paginator;
      // console.log(this.paginator)
      this.cadastros.sort = this.sort;
      this.data = data;
      this.isLoading = false;
    }, error => {
      this.toastr.error('Falha na comunicação com servidor!', 'Erro!');
    });
  }

  postCadastro(cadastro: Cadastro): void {
    this.cadastroService.saveOrder(cadastro).subscribe(() => {
      this.toastr.success('Cadastrado!', 'Sucesso!');
      this.isLoading = true;
      this.getCadastros();
    },err => {
      this.toastr.error('Falha na comunicação com servidor!', 'Erro!');
    });
  }

  deleteCadastro(): void {
    this.cadastroService.deleteOrder(this.cadastroKey)
      .subscribe(() => {
        this.isLoading = true;
        this.getCadastros();
        this.dialog.closeAll();
        this.toastr.success('Deletado!', 'Sucesso!');
      },err => {
        this.toastr.error('Falha na comunicação com servidor!', 'Erro!');
      });
  }

  updateCadastro(cadastro: Cadastro): void {
    this.cadastroService.updateOrder(this.cadastroKey,cadastro)
      .subscribe((ans) => {
        this.toastr.success('Atualizado!', 'Sucesso!');
        this.isLoading = true;
        this.getCadastros();
      },err => {
        this.toastr.error('Falha na comunicação com servidor!', 'Erro!');
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.cadastros.filter = filterValue.trim().toLowerCase();

    if (this.cadastros.paginator) {
      this.cadastros.paginator.firstPage();
    }
  }

  openDialog() {
    this.imgSrc = "";
    this.items().clear();
    this.addItem();
    this.controlSubmit = true;
    this.controlDelete = false;
    this.userform.reset();
    const dialogRef = this.dialog.open(this.customTemplate);
    // dialogRef.afterClosed().subscribe(result => {
    //   console.log(`Dialog result: ${result}`);
    // });
  }

  openDialogEdit(cadastro) {
    cadastro.data = cadastro.data.substring(0, 10);
    this.dataFormItem = cadastro;
    this.items().clear();
    this.dataFormItem.items.forEach(()=>{
      this.editItem();
    });
    var resultado = cadastro.evidencia == undefined ? '' : cadastro.evidencia.substring(5, 20);
    this.controlPreviewFormat = resultado == 'application/pdf' ? true : false;
    this.imgSrc = cadastro.evidencia;
    this.controlSubmit = false;
    this.cadastroKey = cadastro
    this.controlDelete = true;
    this.userform.patchValue(cadastro);
    const dialogRef = this.dialog.open(this.customTemplate);
    // dialogRef.afterClosed().subscribe(result => {
    //   console.log(`Dialog result: ${result}`);
    // });
  }


  getCurrent(event){
    this.isLoading = true;
    this.cadastroService.getOrders(event.pageIndex).subscribe(data => {
      const  {cadastros}  =  data;
      this.cadastros = new MatTableDataSource(cadastros);
      this.isLoading = false;
    }, error => {
      this.toastr.error('Falha na comunicação com servidor!', 'Erro!');
    });
    // const skip = this.paginator.pageSize * this.paginator.pageIndex;
    // const paged = this.data.filter((u, i) => i >= skip)
    // .filter((u, i) => i <this.paginator.pageSize);
    // console.log(paged);
    // this.cadastros = new MatTableDataSource(paged);
   }


}
