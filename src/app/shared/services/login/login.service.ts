import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  isLogin = false;
  roleAs: string;
  userColletion: any;

  constructor(private afAuth: AngularFireAuth, private router: Router) {
  }

  currentUser() {
    return this.afAuth.authState;
  }

  login(email, pwd) {
    return this.afAuth.signInWithEmailAndPassword(email.value, pwd.value);
  }

  resetPwd(email) {
     return this.afAuth.sendPasswordResetEmail(email);
  }
}
