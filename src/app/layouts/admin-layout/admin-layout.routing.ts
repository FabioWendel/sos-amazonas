import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { CadastroComponent } from 'app/cadastro/cadastro.component';
import { LoginComponent } from 'app/login/login.component';
import { NotFoundComponent } from 'app/not-found/not-found.component';
import { AuthGuard } from 'app/auth.guard';

export const AdminLayoutRoutes: Routes = [
    { path: 'cadastro', component: CadastroComponent, canActivate: [AuthGuard]},
    { path: 'dashboard', component: DashboardComponent },
    { path: 'login', component: LoginComponent },
    { path: 'not-found', component: NotFoundComponent },
    { path: '**', redirectTo: 'not-found' },

];
