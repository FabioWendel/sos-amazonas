import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {


  private apiUrl = environment.dataBaseConfig.databaseURL;


  constructor(private httpClient: HttpClient) { }


  getItems(): Observable<any> {
    return this.httpClient.get<any>(`${this.apiUrl}items`)
      .pipe(
        retry(2),
        catchError(this.handleError))
  }

  getItemsMunicipio(municipio): Observable<any> {
    return this.httpClient.get<any>(`${this.apiUrl}items/${municipio}`)
      .pipe(
        retry(2),
        catchError(this.handleError))
  }

  getAllLocal(): Observable<any> {
    return this.httpClient.get<any>(`${this.apiUrl}local`)
      .pipe(
        retry(2),
        catchError(this.handleError))
  }

  getByLocal(local): Observable<any> {
    return this.httpClient.get<any>(`${this.apiUrl}local/${local}`)
      .pipe(
        retry(2),
        catchError(this.handleError))
  }

  getAllItem(): Observable<any> {
    return this.httpClient.get<any>(`${this.apiUrl}allitems`)
      .pipe(
        retry(2),
        catchError(this.handleError))
  }

  getByItem(item): Observable<any> {
    return this.httpClient.get<any>(`${this.apiUrl}allitems/${item}`)
      .pipe(
        retry(2),
        catchError(this.handleError))
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };
}
