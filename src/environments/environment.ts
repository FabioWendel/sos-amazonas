// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  apiIbge:{
    databaseURL: "https://servicodados.ibge.gov.br/api/v1/localidades/estados/am/municipios"
  },
  dataBaseConfig: {
    databaseURL: "http://sosam.kinghost.net:21172/",
  },
  firebaseConfig: {
    apiKey: "AIzaSyDGWLAw2FA4_fhJ8tZNyRjM-ZIbpoe6dsU",
    authDomain: "sos-am.firebaseapp.com",
    databaseURL: "https://sos-am-default-rtdb.firebaseio.com",
    projectId: "sos-am",
    storageBucket: "sos-am.appspot.com",
    messagingSenderId: "179553947059",
    appId: "1:179553947059:web:a9f23baff5799c6ee1cfc6"
  }

};
