import { Component, OnInit, } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'app/shared/services/login/login.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {



  constructor(public router: Router, 
              private loginService: LoginService,
              private toastr: ToastrService) { }

  ngOnInit(): void {
  }


  onLoggedin(email, pwd) {
    this.loginService.login(email, pwd).then(ok => {
      localStorage.setItem('isLoggedin', 'true');
      this.router.navigate(["/cadastro"]);
    }).catch( (error) => {
      var errorCode = error.code;
      var errorMessage = error.message;
      switch (errorCode) {
        case 'auth/wrong-password': {
          this.toastr.error('Senha inválida, tente novamente', 'Erro!');
          return
        }
        case 'auth/invalid-email': {
          this.toastr.error(`Usuário inválido.\n${email.value}, tente novamente'`, 'Erro!');
          return
        }
        case 'auth/user-disabled': {
          this.toastr.error('Usuário desabilitado.', 'Erro!');

          return
        }
        case 'auth/user-not-found': {
          this.toastr.error(`Usuário não encontrado.\n${email.value}`, 'Erro!');
          return
        }
        default: this.toastr.error('Usuário não encontrado. '+ errorMessage, 'Erro!');
      }
    });
  }

  forgetPwd(email) {
    if (!email.value) { 
      this.toastr.error('Informe seu e-mail.', 'Erro!');
      return 
    }
    this.loginService.resetPwd(email.value).then( () => {
      this.toastr.success('Foi enviado um e-mail para ' + email.value +' com instruções para redefinir a sua senha.', 'Sucesso!');
    }).catch( (error) => {
      this.toastr.error('Não foi possível enviar e-mail, entre em contato com o administrador do sistema.', 'Erro!');
    });
  }

}
