export const environment = {
  production: true,
  apiIbge:{
    databaseURL: "https://servicodados.ibge.gov.br/api/v1/localidades/estados/am/municipios"
  },
  firebaseConfig: {
    apiKey: "AIzaSyDGWLAw2FA4_fhJ8tZNyRjM-ZIbpoe6dsU",
    authDomain: "sos-am.firebaseapp.com",
    databaseURL: "https://sos-am-default-rtdb.firebaseio.com",
    projectId: "sos-am",
    storageBucket: "sos-am.appspot.com",
    messagingSenderId: "179553947059",
    appId: "1:179553947059:web:a9f23baff5799c6ee1cfc6"
  }
};
