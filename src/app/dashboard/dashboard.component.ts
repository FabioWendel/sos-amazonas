import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSelect } from '@angular/material/select';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Dashboard } from 'app/shared/models/dashboard/dashboard.model';
import { DashboardService } from 'app/shared/services/dashboard/dashboard.service';
import { Chart } from 'chart.js';
import { ToastrService } from 'ngx-toastr';
import { iterator } from 'rxjs/internal-compatibility';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  displayedColumns = ['name', 'local', 'medida', 'totalItems'];

  displayedColumnsLocal = ['name','municipio', 'medida', 'totalItems'];

  displayedColumnsItems = ['local','municipio', 'medida', 'totalItems'];



  canvasGeral: any;
  canvasItems: any;

  ctxGeral: any;
  ctxItems: any;

  @ViewChild('myChartGeral') mychartGeral;
  @ViewChild('myChartItems') mychartItems;


  timeDate = new Date();
  data: any;
  isLoading = true;

  labelsLocais: any;
  labelsMunicipios: any;
  isLoadingItems = true;
  isMessageSelectedMunicipio = true;
  dataItemsMunicipio: any;
  myChartItems: Chart;
  selectedMunicipio: any;
  dataItemsTable: MatTableDataSource<Dashboard>;
  dataItemsTableLocais: MatTableDataSource<any>;
  dataItemsTableItems: MatTableDataSource<any>;
  isLoadingLocais = true;
  selectedLocal: any;
  labelsItems: any;
  selectedItem: any;
  isLoadingItem = true;
  filteredListMunicipios: any[]
  filteredListLocal: any[]
  filteredListItems: any[]


  constructor(
    private dashboardService: DashboardService,
    private toastr: ToastrService,
  ) { }


  ngOnInit() {
      this.getItems();
      this.getLabelsLocal();
      this.getLabelsItem();
  }

  // compareObjects(municipio: any): boolean {
  //   console.log( municipio)
  //   return municipio;
  // }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataItemsTable.filter = filterValue.trim().toLowerCase();
  }

  applyFilterLocal(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataItemsTableLocais.filter = filterValue.trim().toLowerCase();
  }

  applyFilterItems(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataItemsTableItems.filter = filterValue.trim().toLowerCase();
  }

  getItems(): void {
    this.dashboardService.getItems().subscribe(data => {
      this.data = data;
      this.isLoading = false;
      this.createChartGeral();
    }, error => {
      this.toastr.error('Falha na comunicação com servidor!', 'Erro!');
    });
  }

  getLabelsLocal(): void {
    this.dashboardService.getAllLocal().subscribe(data => {
      this.labelsLocais = data;
      this.filteredListLocal =  data;
    }, error => {
      this.toastr.error('Falha na comunicação com servidor!', 'Erro!');
    });
  }


  getLabelsItem(): void {
    this.dashboardService.getAllItem().subscribe(data => {
      this.labelsItems = data;
      this.filteredListItems = data;
    }, error => {
      this.toastr.error('Falha na comunicação com servidor!', 'Erro!');
    });
  }

  getItemsMunicipio( municipio ) {
    this.isLoadingItems = true;
    this.selectedMunicipio = municipio;
    this.dashboardService.getItemsMunicipio(municipio).subscribe(data => {
      this.isLoadingItems = false;
      this.dataItemsMunicipio = data;
      this.dataItemsTable = new MatTableDataSource(data);
      this.isMessageSelectedMunicipio = false;
      // this.createChartItems();
    }, error => {
      this.toastr.error('Falha na comunicação com servidor!', 'Erro!');
    });
  }

  getItemsByLocal( local ){
    this.isLoadingLocais = true;
    this.selectedLocal = local;
    this.dashboardService.getByLocal(local).subscribe(data => {
      this.isLoadingLocais = false;
      this.dataItemsTableLocais = new MatTableDataSource(data);
    }, error => {
      this.toastr.error('Falha na comunicação com servidor!', 'Erro!');
    });
  }

  getByLocalItems( item ){
    this.isLoadingItem = true;
    this.selectedItem = item;
    this.dashboardService.getByItem(item).subscribe(data => {
      this.isLoadingItem = false;
      this.dataItemsTableItems = new MatTableDataSource(data);
    }, error => {
      this.toastr.error('Falha na comunicação com servidor!', 'Erro!');
    });
  }

  getChartLabelsGeral(){
    var labels = [];

    const { municipio } = this.data;

    municipio.forEach((data,i) =>{
         labels.push(municipio[i].municipio);
    })
    this.labelsMunicipios = labels;
    this.filteredListMunicipios = labels;
    return labels;
  }

  getDatasetsGeral() {
    this.timeDate = new Date();
    let datasets = {};
    let dataData = [];

    const { municipio } = this.data;

    municipio.forEach((data, i) => {
      dataData.push(municipio[i].totalItemsDoados)
    });

    datasets = (
      {
        backgroundColor: '#55CBCD',
        borderColor: '#555',
        barThickness: 25,
        // categoryPercentage: 1.0,
        // barPercentage: 0.5,
        data: dataData,
        borderWidth: 1
      }
    )
    return datasets;
  }



  createChartGeral() {
    this.canvasGeral = this.mychartGeral.nativeElement;
    this.ctxGeral = this.canvasGeral.getContext('2d');

    let myChart = new Chart(this.ctxGeral, {
      type: 'bar',

      data:{
        labels: this.getChartLabelsGeral(),
        datasets:  [this.getDatasetsGeral()],
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'Total de items doados por municípios',
          fontSize: 25
        },
        scales: {
          xAxes:[{
              ticks:{
                fontSize:17
              }
          }],
          yAxes: [{
            ticks: {
              fontSize: 20,
              beginAtZero:true
            },
            scaleLabel: {
              labelString: 'Quantidade de items',
              display: true,
              fontSize: 22
            }
          }],
        },
        legend: {
          display: false,
      },
      plugins: {
        legend: {
            labels: {
               fontSize:20
            }
        }
    }
        , animation: {
          onComplete: function () {
            if (!this.rectangleSet) {
              var scale = window.devicePixelRatio;

              var sourceCanvas = this.chart.canvas;
              var copyWidth = this.chart.chart.scales['y-axis-0'].width - 10;
              var copyHeight =
                this.chart.chart.scales['y-axis-0'].height + this.scales['y-axis-0'].top + 10;

              var can = <HTMLCanvasElement>document.getElementById('chartAxisGeral');

              if(can != null) {  
                var targetCtx = can.getContext('2d');
                targetCtx.scale(scale, scale);
                targetCtx.canvas.width = copyWidth * scale;
                targetCtx.canvas.height = copyHeight * scale;
  
                targetCtx.canvas.style.width = `${copyWidth}px`;
                targetCtx.canvas.style.height = `${copyHeight}px`;
                targetCtx.drawImage(
                  sourceCanvas,
                  0,
                  0,
                  copyWidth * scale,
                  copyHeight * scale,
                  0,
                  0,
                  copyWidth * scale,
                  copyHeight * scale
                );
  
                var sourceCtx = sourceCanvas.getContext('2d');
  
                // Normalize coordinate system to use css pixels.
  
                sourceCtx.clearRect(0, 0, copyWidth * scale, copyHeight * scale);
                this.rectangleSet = true;
            } else {  
                // report the error.  
                console.log("aqui na rolou ")
            }
            }
          },
          onProgress: function () {
            if (this.rectangleSet === true) {
              var copyWidth = this.chart.chart.scales['y-axis-0'].width;
              var copyHeight =
                this.chart.chart.scales['y-axis-0'].height +
                this.chart.chart.scales['y-axis-0'].top +
                10;

              var sourceCtx = this.chart.canvas.getContext('2d');
              sourceCtx.clearRect(0, 0, copyWidth, copyHeight);
            }
          },
        }
      }
    });
  }



  getChartLabelsItems(){
    var labels = [];

    this.dataItemsMunicipio.forEach((data,i) =>{
         labels.push(this.dataItemsMunicipio[i].name);
    })
    return labels;
  }

  getDatasetsItems() {
    this.timeDate = new Date();
    let datasets = {};
    let dataData = [];

    this.dataItemsMunicipio.forEach((data, i) => {
      dataData.push(this.dataItemsMunicipio[i].totalItems)
    });

    datasets = (
      {
        backgroundColor: '#55CBCD',
        borderColor: '#555',
        barThickness: 10,
        // categoryPercentage: 1.0,
        // barPercentage: 0.5,
        data: dataData,
        borderWidth: 1
      }
    )
    return datasets;
  }


  createChartItems() {
    if(this.myChartItems != null){
      this.myChartItems.destroy();
    }
    this.canvasItems = this.mychartItems.nativeElement;
    this.ctxItems = this.canvasItems.getContext('2d');
    this.myChartItems = new Chart(this.ctxItems, {
      type: 'bar',

      data:{
        labels: this.getChartLabelsItems(),
        datasets:  [this.getDatasetsItems()],
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'Total Tipos de Items',
          fontSize: 25
        },
        scales: {
          xAxes:[{
              ticks:{
                fontSize:10
              }
          }],
          yAxes: [{
            ticks: {
              fontSize: 20,
              beginAtZero:true
            },
            scaleLabel: {
              labelString: 'Quantidade de items',
              display: true,
              fontSize: 22
            }
          }],
        },
        legend: {
          display: false,
      },
      plugins: {
        legend: {
            labels: {
               fontSize:20
            }
        }
    }
        , animation: {
          onComplete: function () {
            if (!this.rectangleSet) {
              var scale = window.devicePixelRatio;

              var sourceCanvas = this.chart.canvas;
              var copyWidth = this.chart.chart.scales['y-axis-0'].width - 10;
              var copyHeight =
                this.chart.chart.scales['y-axis-0'].height + this.scales['y-axis-0'].top + 10;

              var can = <HTMLCanvasElement>document.getElementById('chartAxisItems');
              var targetCtx = can.getContext('2d');
              targetCtx.scale(scale, scale);
              targetCtx.canvas.width = copyWidth * scale;
              targetCtx.canvas.height = copyHeight * scale;

              targetCtx.canvas.style.width = `${copyWidth}px`;
              targetCtx.canvas.style.height = `${copyHeight}px`;
              targetCtx.drawImage(
                sourceCanvas,
                0,
                0,
                copyWidth * scale,
                copyHeight * scale,
                0,
                0,
                copyWidth * scale,
                copyHeight * scale
              );

              var sourceCtx = sourceCanvas.getContext('2d');

              // Normalize coordinate system to use css pixels.

              sourceCtx.clearRect(0, 0, copyWidth * scale, copyHeight * scale);
              this.rectangleSet = true;
            }
          },
          onProgress: function () {
            if (this.rectangleSet === true) {
              var copyWidth = this.chart.chart.scales['y-axis-0'].width;
              var copyHeight =
                this.chart.chart.scales['y-axis-0'].height +
                this.chart.chart.scales['y-axis-0'].top +
                10;

              var sourceCtx = this.chart.canvas.getContext('2d');
              sourceCtx.clearRect(0, 0, copyWidth, copyHeight);
            }
          },
        }
      }
    });
    setTimeout(() => {
      this.isLoadingItems = false;
    }, 1000);
  }

}
