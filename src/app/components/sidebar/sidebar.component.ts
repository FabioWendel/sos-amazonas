import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'app/shared/services/login/login.service';

declare const $: any;
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  { path: '/dashboard', title: 'Dashboard', icon: 'dashboard', class: '' },
  { path: '/cadastro', title: 'Cadastro', icon: 'post_add', class: '' },
  { path: '/not-found', title: 'Not-found', icon: 'post_add', class: '' },
  { path: '/login', title: 'login', icon: 'post_add', class: '' }

];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor(public router: Router, public loginService: LoginService) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.loginService.currentUser().subscribe((user: any) => {
      if (!user) {
        this.onLoggedout();
        this.router.navigateByUrl('/login');
      } else {
      }
    });
  }

  onLoggedout() {
    localStorage.removeItem('isLoggedin');
    this.router.navigateByUrl('/login');
  }
  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  };
}
