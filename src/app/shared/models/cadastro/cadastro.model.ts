export class Cadastro {
  id: string;
  termo: string;
  municipio: string;
  data: string;
  item: string;
  quantidade: string;
  medida: string;
  local: string;
  evidencia: string;
}
